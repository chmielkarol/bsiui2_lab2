package chmiel.karol.bsiui2.lab2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static java.lang.Math.min;
import static java.util.Map.Entry.comparingByValue;

class Compressor {

    private static final int END_OF_FILE = -1;
    private static final int REMAINING_BITS_INFO_SIZE = 3;
    private static final int BITS_IN_BYTE = 8;

    private final String inName;
    private final FileOutputStream out;
    private int compressedByteSize;
    private Map<Integer, Integer> dictionary;
    private int fileSize;

    Compressor(String inName, String outName) throws FileNotFoundException {
        this.inName = inName;
        out = new FileOutputStream(outName);
        dictionary = new HashMap<>();
    }

    private static int log2Ceil(int x) {
        return (int) Math.ceil((Math.log(x) / Math.log(2)));
    }

    void prepareDictionary() throws IOException {
        FileInputStream in = new FileInputStream(inName);
        fileSize = 0;

        int currentByte;
        while ((currentByte = in.read()) != END_OF_FILE) {
            ++fileSize;
            printNumberOfProcessedMB(fileSize);
            dictionary.computeIfAbsent(currentByte, k -> dictionary.size());
        }
        in.close();

        compressedByteSize = log2Ceil(dictionary.size());

        writeDictionaryInfoToFile();
    }

    void compress() throws IOException {
        FileInputStream in = new FileInputStream(inName);

        byte bitsToAddAtTheEnd = (byte) ((BITS_IN_BYTE - ((fileSize * compressedByteSize + REMAINING_BITS_INFO_SIZE) % BITS_IN_BYTE)) % BITS_IN_BYTE);

        byte currentlyWrittenByte = bitsToAddAtTheEnd;
        int bitsRemainingInCurrentlyWrittenByte = BITS_IN_BYTE - REMAINING_BITS_INFO_SIZE;

        int currentlyReadByte;
        int processedBytes = 0;
        while ((currentlyReadByte = in.read()) != END_OF_FILE) {
            ++processedBytes;
            printNumberOfProcessedMB(processedBytes);

            int compressedCurrentlyReadByte = compressValue(currentlyReadByte);

            int bitsRemainingToAdd = compressedByteSize;
            while (bitsRemainingToAdd > 0) {
                int bitsToAddInCurrentlyWrittenByte = min(bitsRemainingInCurrentlyWrittenByte, bitsRemainingToAdd);
                bitsRemainingToAdd -= bitsToAddInCurrentlyWrittenByte;

                int valueToAddInCurrentlyWrittenByte = compressedCurrentlyReadByte >> bitsRemainingToAdd;
                compressedCurrentlyReadByte = compressedCurrentlyReadByte - (valueToAddInCurrentlyWrittenByte << bitsRemainingToAdd);

                currentlyWrittenByte = (byte) (currentlyWrittenByte << bitsToAddInCurrentlyWrittenByte);
                currentlyWrittenByte += valueToAddInCurrentlyWrittenByte;
                bitsRemainingInCurrentlyWrittenByte -= bitsToAddInCurrentlyWrittenByte;

                if (bitsRemainingInCurrentlyWrittenByte <= 0) {
                    out.write(currentlyWrittenByte);

                    currentlyWrittenByte = 0;
                    bitsRemainingInCurrentlyWrittenByte = BITS_IN_BYTE;
                }
            }
        }

        if (bitsToAddAtTheEnd > 0) {
            writeLastByteToFileFillingMissingBitsWithOnes(bitsToAddAtTheEnd, currentlyWrittenByte);
        }

        in.close();
        out.close();
    }

    private void writeLastByteToFileFillingMissingBitsWithOnes(byte bitsToAddAtTheEnd, byte currentlyWrittenByte) throws IOException {
        currentlyWrittenByte = (byte) (currentlyWrittenByte << bitsToAddAtTheEnd);
        currentlyWrittenByte += (byte) Math.pow(2, bitsToAddAtTheEnd) - 1;
        out.write(currentlyWrittenByte);
    }

    private void printNumberOfProcessedMB(int fileSize) {
        if (fileSize % 1048576 == 0) {
            System.out.println("Processed: " + fileSize / 1048576 + " MB");
        }
    }

    private void writeDictionaryInfoToFile() throws IOException {
        out.write((byte) dictionary.size());

        dictionary.entrySet()
                .stream()
                .sorted(comparingByValue())
                .forEach(writeDictionaryElementToFile());
    }

    private Consumer<Map.Entry<Integer, Integer>> writeDictionaryElementToFile() {
        return element -> {
            try {
                out.write(element.getKey().byteValue());
            } catch (IOException ignored) {
            }
        };
    }

    private int compressValue(int key) {
        dictionary.computeIfAbsent(key, k -> dictionary.size());
        return dictionary.get(key);
    }
}
